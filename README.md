# Estació meteorològica
Projecte d'Arduino per fer una estació meteorològica per al meu hort.
 
## Característiques
- Enregistrar: pressió temperatura i humitat.
 
## Components electrònics
- Sensor BME230
- Lector de microSD
- Rellotge DS231


Imatge del projecte:
https://wiki.openstreetmap.org/wiki/File:Hybrid_Black_Poplar_leaf-1024px.svg