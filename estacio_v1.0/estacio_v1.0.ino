// Arduino Nano
#include <SD.h>
#include <SPI.h> 
#include <Wire.h>
#include <Adafruit_BME280.h>
#include "RTClib.h"

// Inicialitzacions diverses
RTC_DS3231 rtc;
Adafruit_BME280  bme;
File arxiu;

// Variables per a ara
char hui[13];
char hora[8];

// Pin que rebrà l'alarma, SQW
const int alarmPin = 2;

void setup() {
  Serial.begin(9600);
  // Inicialització de la BME280
  if (!bme.begin(0x76)) {
    Serial.println(F("Error amb la BME280"));
    delay(100);
    abort();
  }

  // Inicialització de la microSD
  if ( !SD.begin(10) )
  {
    Serial.println(F("Error amb l'SD"));
    delay(100);
    abort();
  }
  
  // Definició del rellotge i l'alarma
  if (!rtc.begin()) {
    Serial.println(F("Error amb el rellotge"));
    Serial.flush();
    //abort();
  }
  pinMode(alarmPin, INPUT_PULLUP); // Set alarm pin as pullup
  rtc.disableAlarm(1);
  rtc.disableAlarm(2);
  rtc.clearAlarm(1);
  rtc.clearAlarm(2);
  rtc.writeSqwPinMode(DS3231_OFF); // Place SQW pin into alarm interrupt mode
  rtc.disable32K();
  DateTime now = rtc.now(); // Get current time
  rtc.setAlarm1(now + TimeSpan(0, 0, 0, 10), DS3231_A1_Second);
  
  setNomArxiu();
}

void loop() {
  // Definició de l'arxiu de hui
  DateTime ara = rtc.now();
  // Quan l'alarma s'activa l'ona quadrada s'apaga
  if (digitalRead(alarmPin) == LOW) {
      Serial.println("LOW");
      setNomArxiu();
  }
  arxiu = SD.open(hui, FILE_WRITE);
  
  // Enregistrament de les dades
  sprintf(hora, "%02d:%02d", ara.hour(), ara.minute());
  arxiu.print(hora);
  arxiu.print("\t");
  arxiu.print(bme.readTemperature());
  arxiu.print("\t");
  arxiu.print(bme.readHumidity());
  arxiu.print("\t");
  arxiu.println(bme.readPressure()/100);
  arxiu.close();
  // Cinc minuts
  delay(300000);
}

void setNomArxiu() {
  DateTime ara = rtc.now();
  sprintf(hui, "%04d%02d%02d.txt", ara.year(), ara.month(), ara.day());
  arxiu = SD.open(hui, FILE_WRITE);
  arxiu.println(F("####### EN PROVES ### versió: 0.1 ######"));
  arxiu.println(F("Hora\tTemperatura(ºC)\tHumitat(%)\tPressió(hPa)"));
  rtc.disableAlarm(1);
  rtc.clearAlarm(1);
  rtc.setAlarm1(DateTime(2020, 12, 25, 0, 0, 0), DS3231_A1_Hour);
  arxiu.close();
}
